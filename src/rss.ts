import { RssFeed, RssItem } from './types';

export async function loadRSSFeed(url: string): Promise<RssFeed> {
  const rsp = await fetch(url);
  const txt = await rsp.text();
  const domParser = new DOMParser();
  const doc = domParser.parseFromString(txt, 'text/xml');
  const feed: RssFeed = {
    items: [],
  };

  const getProperty = (node: Element, property: string) => {
    const propNode = node.querySelector(property);
    if (propNode) {
      return propNode.textContent ?? '';
    }
    return '';
  };

  const blank = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D';
  const blanknull = '//:0';
  const xprop = "*[local-name()='content']/@medium";
  const xpath = "*[local-name()='content']/@url";
  const xpropgroup = "*[local-name()='group']";

  const getXpathProperty = (doc: Document, node: Element, property: string): string => {
    let propResult = doc.evaluate(xprop, node, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    if (propResult.snapshotItem(0)?.textContent === property) {
      let result = doc.evaluate(xpath, node, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
      return result.snapshotItem(0)?.textContent ?? blank;
    } else {
      let propResultGroup = doc.evaluate(xpropgroup, node, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
      if (propResultGroup.snapshotLength === 1) {
        let resultGroup = node.querySelector('group') ?? null;
        if (resultGroup !== null) {
          let recursiveSearch = getXpathProperty(doc, resultGroup, property);
          return recursiveSearch ?? blank;
        } else {
          return blanknull;
        }
      }
      return blanknull;
    }
  };

  doc.querySelectorAll('item').forEach((node) => {
    const item: RssItem = {
      title: getProperty(node, 'title'),
      link: getProperty(node, 'link'),
      content: getProperty(node, 'description'),
      pubDate: getProperty(node, 'pubDate'),
      image: getXpathProperty(doc, node, 'image'),
      video: getXpathProperty(doc, node, 'video'),
      category: getProperty(node, 'category'),
    };

    feed.items.push(item);
  });

  return feed;
}
