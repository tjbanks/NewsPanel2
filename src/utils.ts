import { RssFeed } from './types';
import { ArrayVector, FieldType, DataFrame, dateTime } from '@grafana/data';

export function feedToDataFrame(feed: RssFeed): DataFrame {
  const date = new ArrayVector<number>([]);
  const title = new ArrayVector<string>([]);
  const link = new ArrayVector<string>([]);
  const content = new ArrayVector<string>([]);
  const image = new ArrayVector<string>([]);
  const video = new ArrayVector<string>([]);
  const category = new ArrayVector<string>([]);

  for (const item of feed.items) {
    const val = dateTime(item.pubDate);

    try {
      date.buffer.push(val.valueOf());
      title.buffer.push(item.title);
      link.buffer.push(item.link);

      if (item.content) {
        const body = item.content.replace(/<\/?[^>]+(>|$)/g, '');
        content.buffer.push(body);
      }
      if (item.image) {
        const img = item.image.replace('http://','https://')
        image.buffer.push(img);
      }
      if (item.video) {
        const vid = item.video.replace('http://','https://')
        video.buffer.push(vid);
      }
      if (item.category) {
        category.buffer.push(item.category);
      }
    } catch (err) {
      console.warn('Error reading news item:', err, item);
    }
  }

  return {
    fields: [
      { name: 'date', type: FieldType.time, config: { displayName: 'Date' }, values: date },
      { name: 'title', type: FieldType.string, config: {}, values: title },
      { name: 'link', type: FieldType.string, config: {}, values: link },
      { name: 'content', type: FieldType.string, config: {}, values: content },
      { name: 'image', type: FieldType.string, config: {}, values: image },
      { name: 'video', type: FieldType.string, config: {}, values: video },
      { name: 'category', type: FieldType.string, config: {}, values: category },
    ],
    length: date.length,
  };
}
