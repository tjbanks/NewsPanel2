# News Panel Plugin for Grafana
Derivative of News Panel Plugin to include additional features

VUCA 2021

## Development

Clone this repository into a grafana-plugins directory
```
mkdir grafana-plugins
cd grafana-plugins
git clone https://gitlab.com/tjbanks/NewsPanel2
cd ..
```

Run a temporary Grafana instance in docker
```
docker run -d -p 3000:3000 -v "$(pwd)"/grafana-plugins:/var/lib/grafana/plugins --name=grafana grafana/grafana:7.0.0
```

Restart when plugins change.
```
docker restart grafana
```

The plugin will need to be built and run in dev mode, see Extras for Windows.
```
yarn install
```

Anytime you make a change to the plugin files you'll need to run
```
yarn dev
```

and reload the grafana page.

Visit (http://localhost:3000)[http://locahost:3000] and modify the plugin files.

Reference: https://grafana.com/tutorials/build-a-panel-plugin/

## Building

Install yarn
```
npm install --global yarn
```

Build
```
yarn install --pure-lockfile
yarn build
```

Sign (not optional after newer Grafana versions)
```
export GRAFANA_API_KEY=<VUCA_API_KEY> (or set GRAFANA_API_KEY=<VUCA_API_KEY> for windows)
npx @grafana/toolkit plugin:sign --rootUrls https://dashboard.vucanews.com,https://dashboard.test.vucanews.com,https://dashboard.dev.vucanews.com

```

Zip
```
zip vucanews-newspanel2-1.0.1.zip dist -r
```
## Extras

Development in Windows WSL environment

It seems to be best to use yarn and npm from within the Windows installation. Map a network drive to `\\wsl$\Ubuntu` and execute from the command line.

### grafana-toolkit dev Error

```
>yarn dev
yarn run v1.22.17
$ grafana-toolkit plugin:dev
'grafana-toolkit' is not recognized as an internal or external command,
operable program or batch file.
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.

```

Fix
```
yarn global add @grafana/toolkit
yarn dev
```